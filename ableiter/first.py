import numpy as np


class First:
    """
    This class can be used to compute the partial
    first-order derivatives of
    a multivariate function.
    """

    def __init__(self, f, n, x_eps=1e-8):
        """
        Sets the function f as the function whose
        partial derivatives are calculated, and n
        is the dimension of the input array of f.
        """

        self.f = f
        self.n = n

        self.x_eps = x_eps

        self.grf = np.zeros(shape=(n, ))

    def df_dxi(self, y, i):
        """
        Return the value of the first derivative with
        respect to the i-th variable in the point y.
        """

        x = np.copy(y)

        if len(x) != self.n:
            raise ValueError

        f = abs(self.f(x))
        if f > 1e0:
            x_eps = self.x_eps * f**0.5
        else:
            x_eps = self.x_eps

        xiold = x[i]

        x[i] = xiold - x_eps
        f1 = self.f(x)

        x[i] = xiold + x_eps
        f2 = self.f(x)

        return 0.5 * (f2 - f1) / x_eps

    def gradf(self, x):
        """
        Return the gradient in the point x
        as a numpy array with length n.
        """

        for i in range(0, self.n):
            self.grf[i] = self.df_dxi(x, i)

        return self.grf
