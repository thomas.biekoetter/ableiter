import numpy as np


class Second:
    """
    This class can be used to compute partial
    second-order derivatives of
    a multivariate function.
    """

    def __init__(self, f, n, x_eps=1e-6):
        """
        Sets the function f as the function whose
        partial derivatives are calculated, and n
        is the dimension of the input array of f.
        """

        self.f = f
        self.n = n

        self.x_eps = x_eps

    def d2f_dxidxj(self, y, i, j):
        """
        Return the value of the second derivative
        in the point y,
        where first the function is derived with
        respect to the i-th variable, and then
        the functions is derived with respect
        to the j-th variable.
        """

        x = np.copy(y)

        if len(x) != self.n:
            raise ValueError

        f = abs(self.f(x))
        if f > 1e0:
            x_eps = self.x_eps * f**0.5
        else:
            x_eps = self.x_eps

        if i != j:

            xiold = x[i]
            xjold = x[j]

            x[i] = xiold - x_eps
            x[j] = xjold - x_eps
            fmm = self.f(x)

            # x[i] = xiold - x_eps
            x[j] = xjold + x_eps
            fmp = self.f(x)

            x[i] = xiold + x_eps
            # x[j] = xjold + x_eps
            fpp = self.f(x)

            # x[i] = xiold + x_eps
            x[j] = xjold - x_eps
            fpm = self.f(x)

            y = 0.25 * (fpp - fpm - fmp + fmm) / x_eps**2

        else:

            xiold = x[i]

            f0 = self.f(x)

            x[i] = xiold - x_eps
            fm = self.f(x)

            x[i] = xiold + x_eps
            fp = self.f(x)

            y = -(2. * f0 - fm - fp) / x_eps**2

        return y
