import numpy as np


class Third:
    """
    This class can be used to compute partial
    third-order derivatives of
    a multivariate function.
    """

    def __init__(self, f, n, x_eps=1e-4):
        """
        Sets the function f as the function whose
        partial derivatives are calculated, and n
        is the dimension of the input array of f.
        """

        self.f = f
        self.n = n

        self.x_eps = x_eps

    def d3f_dxidxjdxk(self, y, i, j, k):
        """
        Return the value of the third derivative
        in the point y,
        where first the function is derived with
        respect to the i-th variable, then
        the functions is derived with respect
        to the j-th variable, and then the
        function is derived with respect to
        the k-th variable.
        """

        x = np.copy(y)

        if len(x) != self.n:
            raise ValueError

        f = abs(self.f(x))
        if f > 1e0:
            x_eps = self.x_eps * f**0.5
        else:
            x_eps = self.x_eps

        if (i == j) and (j == k):

            xiold = x[i]

            x[i] = xiold - 2. * x_eps
            fmm = self.f(x)

            x[i] = xiold - x_eps
            fm0 = self.f(x)

            x[i] = xiold + x_eps
            fp0 = self.f(x)

            x[i] = xiold + 2. * x_eps
            fpp = self.f(x)

            y = 0.5 * (2. * fm0 - 2. * fp0 - fmm + fpp) / \
                x_eps**3

        elif (i == j) or (j == k) or (i == k):

            if i == j:

                il = i
                ir = k

            elif j == k:

                il = j
                ir = i

            else:

                il = i
                ir = j

            xilold = x[il]
            xirold = x[ir]

            x[il] = xilold + 2. * x_eps
            x[ir] = xirold + x_eps
            f21 = self.f(x)

            x[il] = xilold
            x[ir] = xirold + x_eps
            f01 = self.f(x)

            x[il] = xilold - 2. * x_eps
            x[ir] = xirold + x_eps
            fm21 = self.f(x)

            x[il] = xilold + 2. * x_eps
            x[ir] = xirold - x_eps
            f2m1 = self.f(x)

            x[il] = xilold
            x[ir] = xirold - x_eps
            f0m1 = self.f(x)

            x[il] = xilold - 2. * x_eps
            x[ir] = xirold - x_eps
            fm2m1 = self.f(x)

            y = 0.125 * (f21 - 2. * f01 + fm21 - f2m1 + \
                2. * f0m1 - fm2m1) / x_eps**3

        else:

            xiold = x[i]
            xjold = x[j]
            xkold = x[k]

            x[i] = xiold + x_eps
            x[j] = xjold + x_eps
            x[k] = xkold + x_eps
            fppp = self.f(x)

            x[i] = xiold - x_eps
            x[j] = xjold + x_eps
            x[k] = xkold + x_eps
            fmpp = self.f(x)

            x[i] = xiold + x_eps
            x[j] = xjold - x_eps
            x[k] = xkold + x_eps
            fpmp = self.f(x)

            x[i] = xiold - x_eps
            x[j] = xjold - x_eps
            x[k] = xkold + x_eps
            fmmp = self.f(x)

            x[i] = xiold + x_eps
            x[j] = xjold + x_eps
            x[k] = xkold - x_eps
            fppm = self.f(x)

            x[i] = xiold - x_eps
            x[j] = xjold + x_eps
            x[k] = xkold - x_eps
            fmpm = self.f(x)

            x[i] = xiold + x_eps
            x[j] = xjold - x_eps
            x[k] = xkold - x_eps
            fpmm = self.f(x)

            x[i] = xiold - x_eps
            x[j] = xjold - x_eps
            x[k] = xkold - x_eps
            fmmm = self.f(x)

            y = 0.125 * (fppp - fmpp - fpmp + fmmp
                - fppm + fmpm + fpmm - fmmm) / x_eps**3

        return y
