import numpy as np


class Fourth:
    """
    This class can be used to compute partial
    fourth-order derivatives of
    a multivariate function.
    """

    def __init__(self, f, n, x_eps=1e-3):
        """
        Sets the function f as the function whose
        partial derivatives are calculated, and n
        is the dimension of the input array of f.
        """

        self.f = f
        self.n = n

        self.x_eps = x_eps

    def d4f_dxidxjdxkdxl(self, y, i, j, k, l):
        """
        Return the value of the fourth derivative
        in the point y,
        where first the function is derived with
        respect to the i-th variable, then
        the functions is derived with respect
        to the j-th variable, then the
        function is derived with respect to
        the k-th variable, and then the function
        is derived with respect to the l-th variable.
        """

        x = np.copy(y)

        if len(x) != self.n:
            raise ValueError

        f = abs(self.f(x))
        if f > 1e0:
            x_eps = self.x_eps * f**0.5
        else:
            x_eps = self.x_eps

        if len(set([i, j, k, l])) == 1:

            complexity = 1

        elif (i == j) and (k == l) and (i != k):

            il = i
            ir = k
            complexity = 2

        elif (i == k) and (j == l) and (i != j):

            il = i
            ir = j
            complexity = 2

        elif (i == l) and (j == k) and (i != j):

            il = i
            ir = j
            complexity = 2

        elif (i == j) and (i == k) and (i != l):

            il = i
            ir = l
            complexity = 3

        elif (i == j) and (i == l) and (i != k):

            il = i
            ir = k
            complexity = 3

        elif (i == k) and (i == l) and (i != j):

            il = i
            ir = j
            complexity = 3

        elif (i != j) and (j == k) and (j == l):

            il = j
            ir = i
            complexity = 3

        elif len(set([i, j, k, l])) == 4:

            complexity = 4

        elif (i == j) and (len(set([i, j, k, l])) == 3):

            il = i
            im = k
            ir = l
            complexity = 5

        elif (i == k) and (len(set([i, j, k, l])) == 3):

            il = i
            im = j
            ir = l
            complexity = 5

        elif (i == l) and (len(set([i, j, k, l])) == 3):

            il = i
            im = j
            ir = k
            complexity = 5

        elif (j == k) and (len(set([i, j, k, l])) == 3):

            il = j
            im = i
            ir = l
            complexity = 5

        elif (j == l) and (len(set([i, j, k, l])) == 3):

            il = j
            im = i
            ir = k
            complexity = 5

        elif (k == l) and (len(set([i, j, k, l])) == 3):

            il = k
            im = i
            ir = j
            complexity = 5

        else:

            raise NotImplementedError

        if complexity == 2:

            xilold = x[il]
            xirold = x[ir]

            x[il] = xilold + x_eps
            x[ir] = xirold + x_eps
            f11 = self.f(x)

            x[il] = xilold
            x[ir] = xirold + x_eps
            f01 = self.f(x)

            x[il] = xilold - x_eps
            x[ir] = xirold + x_eps
            fm11 = self.f(x)

            x[il] = xilold + x_eps
            x[ir] = xirold
            f10 = self.f(x)

            x[il] = xilold
            x[ir] = xirold
            f00 = self.f(x)

            x[il] = xilold - x_eps
            x[ir] = xirold
            fm10 = self.f(x)

            x[il] = xilold + x_eps
            x[ir] = xirold - x_eps
            f1m1 = self.f(x)

            x[il] = xilold
            x[ir] = xirold - x_eps
            f0m1 = self.f(x)

            x[il] = xilold - x_eps
            x[ir] = xirold - x_eps
            fm1m1 = self.f(x)

            y = (f11 - 2. * f01 + fm11
                - 2. * f10 + 4. * f00 - 2. * fm10
                    + f1m1 - 2. * f0m1 + fm1m1) / \
                        x_eps**4

        elif complexity == 1:

            xiold = x[i]

            f0 = self.f(x)

            x[i] = xiold + 2. * x_eps
            f2 = self.f(x)

            x[i] = xiold + x_eps
            f1 = self.f(x)

            x[i] = xiold - x_eps
            fm1 = self.f(x)

            x[i] = xiold - 2. * x_eps
            fm2 = self.f(x)

            y = (f2 - 4. * f1 + 6. * f0
                - 4. * fm1 + fm2) / x_eps**4

        elif complexity == 3:

            xilold = x[il]
            xirold = x[ir]

            x[il] = xilold + 3. * x_eps
            x[ir] = xirold + x_eps
            f13 = self.f(x)

            # x[il] = xilold + 3. * x_eps
            x[ir] = xirold - x_eps
            fm13 = self.f(x)

            x[il] = xilold + x_eps
            x[ir] = xirold + x_eps
            f11 = self.f(x)

            # x[il] = xilold + x_eps
            x[ir] = xirold - x_eps
            fm11 = self.f(x)

            x[il] = xilold - x_eps
            x[ir] = xirold + x_eps
            f1m1 = self.f(x)

            # x[il] = xilold - x_eps
            x[ir] = xirold - x_eps
            fm1m1 = self.f(x)

            x[il] = xilold - 3. * x_eps
            x[ir] = xirold + x_eps
            f1m3 = self.f(x)

            # x[il] = xilold - 3. * x_eps
            x[ir] = xirold - x_eps
            fm1m3 = self.f(x)

            y = 0.0625 * (f13 - fm13 - 3. * (f11 -
                fm11) + 3. * (f1m1 - fm1m1) - (f1m3 -
                    fm1m3)) / x_eps**4

        elif complexity == 4:

            xiold = x[i]
            xjold = x[j]
            xkold = x[k]
            xlold = x[l]

            x[i] = xiold + x_eps
            x[j] = xjold + x_eps
            x[k] = xkold + x_eps
            x[l] = xlold + x_eps
            f1111 = self.f(x)

            # x[i] = xiold + x_eps
            # x[j] = xjold + x_eps
            # x[k] = xkold + x_eps
            x[l] = xlold - x_eps
            fm1111 = self.f(x)

            # x[i] = xiold + x_eps
            # x[j] = xjold + x_eps
            x[k] = xkold - x_eps
            x[l] = xlold + x_eps
            f1m111 = self.f(x)

            # x[i] = xiold + x_eps
            # x[j] = xjold + x_eps
            # x[k] = xkold - x_eps
            x[l] = xlold - x_eps
            fm1m111 = self.f(x)

            # x[i] = xiold + x_eps
            x[j] = xjold - x_eps
            x[k] = xkold + x_eps
            x[l] = xlold + x_eps
            f11m11 = self.f(x)

            # x[i] = xiold + x_eps
            # x[j] = xjold - x_eps
            # x[k] = xkold + x_eps
            x[l] = xlold - x_eps
            fm11m11 = self.f(x)

            # x[i] = xiold + x_eps
            # x[j] = xjold - x_eps
            x[k] = xkold - x_eps
            x[l] = xlold + x_eps
            f1m1m11 = self.f(x)

            # x[i] = xiold + x_eps
            # x[j] = xjold - x_eps
            # x[k] = xkold - x_eps
            x[l] = xlold - x_eps
            fm1m1m11 = self.f(x)

            x[i] = xiold - x_eps
            x[j] = xjold + x_eps
            x[k] = xkold + x_eps
            x[l] = xlold + x_eps
            f111m1 = self.f(x)

            # x[i] = xiold - x_eps
            # x[j] = xjold + x_eps
            # x[k] = xkold + x_eps
            x[l] = xlold - x_eps
            fm111m1 = self.f(x)

            # x[i] = xiold - x_eps
            # x[j] = xjold + x_eps
            x[k] = xkold - x_eps
            x[l] = xlold + x_eps
            f1m11m1 = self.f(x)

            # x[i] = xiold - x_eps
            # x[j] = xjold + x_eps
            # x[k] = xkold - x_eps
            x[l] = xlold - x_eps
            fm1m11m1 = self.f(x)

            # x[i] = xiold - x_eps
            x[j] = xjold - x_eps
            x[k] = xkold + x_eps
            x[l] = xlold + x_eps
            f11m1m1 = self.f(x)

            # x[i] = xiold - x_eps
            # x[j] = xjold - x_eps
            # x[k] = xkold + x_eps
            x[l] = xlold - x_eps
            fm11m1m1 = self.f(x)

            # x[i] = xiold - x_eps
            # x[j] = xjold - x_eps
            x[k] = xkold - x_eps
            x[l] = xlold + x_eps
            f1m1m1m1 = self.f(x)

            # x[i] = xiold - x_eps
            # x[j] = xjold - x_eps
            # x[k] = xkold - x_eps
            x[l] = xlold - x_eps
            fm1m1m1m1 = self.f(x)

            y = 0.0625 * ((f1111 - fm1111) - (f1m111 -
                fm1m111) - (f11m11 - fm11m11) + (f1m1m11 -
                    fm1m1m11) - (f111m1 - fm111m1) + (f1m11m1 -
                        fm1m11m1) + (f11m1m1 - fm11m1m1) - (f1m1m1m1 -
                            fm1m1m1m1)) / x_eps**4

        elif complexity == 5:

            xilold = x[il]
            ximold = x[im]
            xirold = x[ir]

            x[il] = xilold + 2. * x_eps
            x[im] = ximold + x_eps
            x[ir] = xirold + x_eps
            f112 = self.f(x)

            # x[il] = xilold + 2 * x_eps
            # x[im] = ximold + x_eps
            x[ir] = xirold - x_eps
            fm112 = self.f(x)

            # x[il] = xilold + 2 * x_eps
            x[im] = ximold - x_eps
            x[ir] = xirold + x_eps
            f1m12 = self.f(x)

            # x[il] = xilold + 2 * x_eps
            # x[im] = ximold - x_eps
            x[ir] = xirold - x_eps
            fm1m12 = self.f(x)

            x[il] = xilold
            x[im] = ximold + x_eps
            x[ir] = xirold + x_eps
            f110 = self.f(x)

            # x[il] = xilold
            # x[im] = ximold + x_eps
            x[ir] = xirold - x_eps
            fm110 = self.f(x)

            # x[il] = xilold
            x[im] = ximold - x_eps
            x[ir] = xirold + x_eps
            f1m10 = self.f(x)

            # x[il] = xilold
            # x[im] = ximold - x_eps
            x[ir] = xirold - x_eps
            fm1m10 = self.f(x)

            x[il] = xilold - 2. * x_eps
            x[im] = ximold + x_eps
            x[ir] = xirold + x_eps
            f11m2 = self.f(x)

            # x[il] = xilold - 2. * x_eps
            # x[im] = ximold + x_eps
            x[ir] = xirold - x_eps
            fm11m2 = self.f(x)

            # x[il] = xilold - 2. * x_eps
            x[im] = ximold - x_eps
            x[ir] = xirold + x_eps
            f1m1m2 = self.f(x)

            # x[il] = xilold - 2. * x_eps
            # x[im] = ximold - x_eps
            x[ir] = xirold - x_eps
            fm1m1m2 = self.f(x)

            y = 0.0625 * (f112 - fm112 - (f1m12 - fm1m12)
                - 2. * (f110 - fm110) + 2. * (f1m10 - fm1m10)
                    + (f11m2 - fm11m2) - (f1m1m2 - fm1m1m2)) / x_eps**4

        return y
