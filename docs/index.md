# Welcome to ableiter

This is the documentation of the
package `ableiter`, a tool for the
numerical differentiation of functions
with several variables using
finite differences.

## Installation

`ableiter` can be installed by
typing the following command
from within the main directory:

```
pip install .
```

## Issues

In case you notice any bug or
otherwise undesired behaviour,
please contact [me](mailto:thomas.biekoetter@desy.de)
or raise an issue in the
[gitlab repository](https://gitlab.com/thomas.biekoetter/ableiter).



