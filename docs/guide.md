## User guide

The package is able to perform
partial derivatives up to order
four with respect to any of the
variables of the function.
First-order derivatives can be
computed with the help of the
class `First`, second-order
derivatives with the class
`Second`, and so on. The usage
of these classes is documented
below.

In order to use the package
`ableiter`, it is required
to define the function to
be derived in the following way.
The function (called f from hereon)
should have one single argument.
This argument shall be a `numpy`
array with one dimension. The index
of the array runs over the variables
of f, i.e. `x[0]` is the first variable
of f, `x[1]` is the second variable,
and so on. The length of the input
array has to be equal to the number
of variables of f.

For instance, to define the function

$$
f(x_1, x_2) = x_1^3 - x_2^2 + x_1^3 x_2^4 + x_2 \log(x_1)
$$

one should type:
```
def f(x):
    return x[0]**3 - x[1]**2 + x[0]**3 * x[1]**4 + x[1] * np.log(x[0])
```

&nbsp;

<span style="font-size:larger;">First</span>
::: ableiter.first.First
    handler: python
    selection:
        members:
            - __init__
            - df_dxi
            - gradf
    rendering:
        show_root_heading: true
        heading_level: 4

&nbsp;

<span style="font-size:larger;">Second</span>
::: ableiter.second.Second
    handler: python
    selection:
        members:
            - __init__
            - d2f_dxidxj
    rendering:
        show_root_heading: true
        heading_level: 4

&nbsp;

<span style="font-size:larger;">Third</span>
::: ableiter.third.Third
    handler: python
    selection:
        members:
            - __init__
            - d3f_dxidxjdxk
    rendering:
        show_root_heading: true
        heading_level: 4

&nbsp;

<span style="font-size:larger;">Fourth</span>
::: ableiter.fourth.Fourth
    handler: python
    selection:
        members:
            - __init__
            - d4f_dxidxjdxkdxl
    rendering:
        show_root_heading: true
        heading_level: 4

