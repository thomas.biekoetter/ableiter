# Example script

An example script can be found in the
`example` folder. It defines a few
functions for which the derivatives
are known analytically. The script
then compares the values obtained
numerically with the exact solutions
by printing them to the terminal.

## Source code

The source code of the example
script `main.py` is the
following:

```
import numpy as np

from ableiter.first import First # First partial derivatives
from ableiter.second import Second # Second partial derivatives
from ableiter.third import Third # Third partial derivatives
from ableiter.fourth import Fourth # Third partial derivatives


def main():

    # Define test function f and its derivatives (known analytically)
    # The argument x has to be a 1D array for the input vector x_i
    def f(x):
        return x[0]**3 - x[1]**2 + x[0]**3 * x[1]**4 + x[1] * np.log(x[0])

    def anadf0(x):
        return 3 * x[0]**2 + 3 * x[0]**2 * x[1]**4 + x[1] / x[0]

    def anadf1(x):
        return -2 * x[1] + 4 * x[0]**3 * x[1]**3 + np.log(x[0])

    def anad2fd01(x):
        return 12 * x[0]**2 * x[1]**3 + 1 / x[0]

    def anad2fd00(x):
        return 6 * x[0] + 6 * x[0] * x[1]**4 - x[1] / x[0]**2

    def anad3fd000(x):
        return 6 + 6 * x[1]**4 + 2 * x[1] / x[0]**3

    def anad3fd100(x):
        return 24 * x[0] * x[1]**3 - 1 / x[0]**2

    def anad3fd110(x):
        return 36 * x[0]**2 * x[1]**2

    def anad4fd1100(x):
        return 72 * x[0] * x[1]**2

    def anad4fd0000(x):
        return -6 * x[1] / x[0]**4

    def anad4fd1000(x):
        return 24 * x[1]**3 + 2 / x[0]**3

    def g(x):
        return x[0] * x[1] * x[2]

    def h(x):
        return x[0] * x[1] * x[2] * np.log(x[3])

    def anad4hd0123(x):
        return 1 / x[3]

    def anad4hd1233(x):
        return -x[0] / x[3]**2

    # Initialize numerical derivatives
    # f is the function to be differentiatied
    # 2 is the length of the input x for f
    ab1 = First(f, 2)
    ab2 = Second(f, 2)
    ab3 = Third(f, 2)
    ab4 = Fourth(f, 2)

    # Test point
    x = np.array([np.pi * 1, -np.pi * 0.1])

    # Check whether results identical

    # df / dx0
    print(anadf0(x), ab1.df_dxi(x, 0))
    print()

    # df / dx1
    print(anadf1(x), ab1.df_dxi(x, 1))
    print()

    # d2f / dx1 dx0
    print(anad2fd01(x), ab2.d2f_dxidxj(x, 1, 0))
    print()

    # d2f / dx0 dx0
    print(anad2fd00(x), ab2.d2f_dxidxj(x, 0, 0))
    print()

    # d3f / dx0 dx0 dx0
    print(anad3fd000(x), ab3.d3f_dxidxjdxk(x, 0, 0, 0))
    print()

    # d3f / dx0 dx1 dx0
    print(anad3fd100(x), ab3.d3f_dxidxjdxk(x, 0, 1, 0))
    print()

    # d3f / dx1 dx0 dx1
    print(anad3fd110(x), ab3.d3f_dxidxjdxk(x, 1, 0, 1))
    print()

    # d4f / dx1 dx1 dx0 dx0
    print(anad4fd1100(x), ab4.d4f_dxidxjdxkdxl(x, 1, 1, 0, 0))
    print()

    # d4f / dx04
    print(anad4fd0000(x), ab4.d4f_dxidxjdxkdxl(x, 0, 0, 0, 0))
    print()

    # d4f / dx1 dx03
    print(anad4fd1000(x), ab4.d4f_dxidxjdxkdxl(x, 1, 0, 0, 0))
    print()

    ab3 = Third(g, 3)
    x = np.ones(shape=(3, ))

    # d3g / dx0 dx1 dx2
    print(1, ab3.d3f_dxidxjdxk(x, 0, 1, 2))
    print()

    ab4 = Fourth(h, 4)
    x = 0.1 * np.ones(shape=(4, ))

    # d4h / dx0 dx1 dx2 dx3
    print(anad4hd0123(x), ab4.d4f_dxidxjdxkdxl(x, 0, 1, 2, 3))
    print()

    # d4h / dx1 dx2 dx3 dx3
    print(anad4hd1233(x), ab4.d4f_dxidxjdxkdxl(x, 1, 2, 3, 3))
    print()


main()
```
