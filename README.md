# Installation

The package can be installed using pip:

```
python -m pip install --user .
```

# Usage

The package is documented [here](https://www.desy.de/~biek/ableiterdocu/site/).
