from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name = "ableiter",
    version = "0.1.0",
    description = "A python package for numerical differentiation.",
    long_description = long_description,
    packages = ["ableiter"],
    author = ["Thomas Biekotter"],
    author_email = [
        "thomas.biekoetter@desy.de"],
    url = "https://gitlab.com/thomas.biekoetter/ableiter",
    include_package_data = True,
    package_data = {"ableiter": ["*"]},
    install_requires = ["numpy"],
    python_requires = '>=3.6',
)
